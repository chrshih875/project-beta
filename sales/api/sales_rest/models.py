from django.db import models
from django.urls import reverse


# Create your models here.
class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone = models.BigIntegerField(unique=True)

    def __str__(self):
        return self.name


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100)
    SOLD = "SOLD"
    UNSOLD = "UNSOLD"
    STATUS = [
        (SOLD, "SOLD"),
        (UNSOLD,"UNSOLD")
    ]
    status = models.CharField(max_length=100, choices=STATUS, default=UNSOLD)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

    def __str__(self):
        return self.vin


class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="sales", on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey(
        SalesPerson, related_name="sales", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        PotentialCustomer, related_name="sales", on_delete=models.CASCADE
    )
    price = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.automobile} for {self.customer}'
