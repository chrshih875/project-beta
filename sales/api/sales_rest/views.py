from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import SalesPerson, PotentialCustomer, SalesRecord, AutomobileVO
from .encoders import (
    SalesPersonEncoder,
    PotentialCustomerEncoder,
    SalesRecordEncoder,
    AutomobileVOEncoder,
)
import json

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_sales_people(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse({"sales_people": salespeople}, encoder=SalesPersonEncoder)
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)
        except:
            response = JsonResponse({"message": "Could not create the sales person"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_potential_customers(request):
    if request.method == "GET":
        potentialcustomers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": potentialcustomers},
            encoder=PotentialCustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(customer, encoder=PotentialCustomerEncoder, safe=False)
        except:
            response = JsonResponse({"message": "Could not create the customer"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        salesrecords = SalesRecord.objects.all()
        return JsonResponse({"sales_records": salesrecords}, encoder=SalesRecordEncoder)
    else:
        content = json.loads(request.body)
        print("Content", content["automobile"])
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            AutomobileVO.objects.filter(vin=content["automobile"]).update(status="SOLD")

            sales_person = SalesPerson.objects.get(
                employee_number=content["sales_person"]
            )
            content["sales_person"] = sales_person

            customer = PotentialCustomer.objects.get(phone=content["customer"])
            content["customer"] = customer
        except:
            response = JsonResponse({"message": "Could not create the sales record"})
            response.status_code = 400
            return response

        salesrecord = SalesRecord.objects.create(**content)
        return JsonResponse(salesrecord, encoder=SalesRecordEncoder, safe=False)


@require_http_methods(["GET"])
def api_automobile_vos(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(autos, encoder=AutomobileVOEncoder, safe=False)

@require_http_methods(["GET"])
def api_show_seller_sales(request, pk):
    if request.method == "GET":
        sales = SalesRecord.objects.filter(sales_person=pk)
        print("SALES:::::", sales)
        return JsonResponse({"sales": sales}, encoder=SalesRecordEncoder, safe=False)
