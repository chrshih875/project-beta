import React, {useState, useEffect} from "react"


function ManufacturerList() {

    const [manufacturer, setManufacturer] = useState([]);

    useEffect (() => {
        const getManufacturer = async () => {
            const response = await fetch ("http://localhost:8100/api/manufacturers/");
            const data = await response.json();
            setManufacturer(data.manufacturers)
        }
        getManufacturer()
    }, []);

    return (
        <>
        <h2>Manufacturers</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturer.map((manufacturer) => {
                    return(
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}


export default ManufacturerList
