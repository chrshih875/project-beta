import React, { useState, useEffect } from 'react'

function SalesPersonList() {

    const [salesPerson, setSalesPerson] = useState([])

    useEffect(() => {
        const getSalesPerson = async () => {
            const response = await fetch("http://localhost:8090/api/salespeople/")
            const data = await response.json()
            setSalesPerson(data.sales_people)
        }
        getSalesPerson()
    }, [])

    return (
        <>
            <h2>Sales People</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                    {salesPerson.map((salesPerson) => {
                        return (
                            <tr key={salesPerson.id}>
                                <td>{salesPerson.name}</td>
                                <td>{salesPerson.employee_number}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}


export default SalesPersonList
