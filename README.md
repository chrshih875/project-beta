# CarCar

Team:
    Chris and Jackie did the Inventory together
* Chris - Service
* Jackie - Sales

## Design

open link to excalidraw for design

https://excalidraw.com/#json=nkYk5sQ267KBW8IE0W1Go,iQstma5nh_hd9Fx3XaRv2g

BEFORE ACCESSING ANY OF THE URL BELOW.
FOLLOW STEPS BELOW:
1. Open terminal & Docker
2. docker volume create beta-data
3. input "docker-compose up --build" in terminal to build necessary images and run containers
4. Once image/container is ready to go, click on Url to access webiste - http://localhost:3000/
You will be able to create and view all needed function here. Another method to create and view your items is in insomnia.
Please see below for correct url paths

If you ever wish to wipe your database and restart:
1. stop all services
2. docker container prune -f
3. docker volume rm beta-data
4. docker volume create beta-data
5. docker-compose up

## Inventory microservice

Anything that involves creating something. Change the "GET" next to the URL  to "POST".
Next, Change the drop down menu of "Body" to "JSON"
Paste example code and see example results.

port: 8100

URL for Insomnia:

List of Manufacturers - 	"GET" 	request to 	http://localhost:8100/api/manufacturers/
Create a New Manufacturer - "POST" 	request to 	http://localhost:8100/api/manufacturers/

try adding something like this:
{
  "name": "Honda"
}

expect something like this:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}

List of Vehicles - 		"GET" 	request to 	http://localhost:8100/api/models/
Create a New Vehicle - 	"POST" 	request to	http://localhost:8100/api/models/

try adding something like this:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

expect something like this:
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Honda"
	}
}

List of Automobiles - 		"GET" 	request to 	http://localhost:8100/api/automobiles/
Create a New Automobile - 	"POST" 	request to 	http://localhost:8100/api/automobiles/

try adding something like this:
** MAKE SURE VIN IS 17 DIGIT/LETTER MAX

{
  "color": "red",
  "year": 2005,
  "vin": "65ADS4FA65",
  "model_id": 1
}

expect something like this:
{
	"href": "/api/automobiles/65ADS4FA65/",
	"id": 3,
	"color": "red",
	"year": 2005,
	"vin": "65ADS4FA65",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Civic",
		"picture_url": "https://assets.hermes.com/is/image/hermesproduct/adada-dots-pif-booties--103143M%2001-front-1-300-0-1000-1000_b.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	}
}

## Service microservice

port: 8080

URL for Insomina:

List of VinVo - 	"GET" 	request to 	http://localhost:8080/api/vinVO/

List of Appointments - 	"GET" 	request to 	http://localhost:8080/api/appointment/
Appointment Create - 	"POST" 	request to 	http://localhost:8080/api/appointment/

try adding something like this:
{
		"vin": "453gfj",
		"customer_name": "Jackie",
		"date_time": "2021-07-27",
		"technician": "12345",
		"reason": "Oil Change"
	}

expect something like this:
{
	"vin": {
		"import_href": null,
		"vin": "453gfj",
		"vip_status": "VIP"
	},
	"customer_name": "Jackie",
	"date_time": "2021-07-27",
	"technician": {
		"name": "Lynda",
		"employee_number": 12345,
		"id": 1
	},
	"reason": "Oil Change",
	"id": 32
}

List Technicians - 			"GET" 	request to 	http://localhost:8080/api/technician/
Create a New Technician - 	"POST" 	request to 	http://localhost:8080/api/technician/

try adding something like this:
MAKE SURE EMPLOYEE_NUMBER IS 4 DIGITS MAX
{
	"name": "Paul",
	"employee_number": 7841
}

expect something like this:
{
	"name": "Paul",
	"employee_number": 7841,
	"id": 2
}

Models:

Appointment - added the required property: vin, customer, date, time, technician, and reason. On top of it I have added a status_progress to track whether the created appointment has been finished or still in progress. This will reflect in the history of appointments that been finsihed.

Technician - a simple model with the required property: name, employee_number; used to create a form for new technician

vinVO - Used poller to grab all existing/new vin added to CarCar invenotry, so that we can have our Appointment have a easier time access our car inventory Vin.



## Sales microservice

port: 8090

URL for Insomnia:

List of Sales People - 		"GET" 	request to 	http://localhost:8090/api/salespeople/
Create a New Sales Person - "POST" 	request to 	http://localhost:8090/api/salespeople/

try adding something like this:

** employee number is unique

{
	"name": "Patrick Star",
	"employee_number": 13242
}

expect something like this:
{
	"name": "Patrick Star",
	"employee_number": 13242,
	"id": 3
}

List of Customers - 	"GET" 	request to 	http://localhost:8090/api/customers/
Create a New Customer - "POST" 	request to 	http://localhost:8090/api/customers/

try adding something like this:

** 	for customer phone number,
	enter 10 digit number WITHOUT hyphens and must be unique

{
	"name": "Craig E. Liriano",
	"address": "3538 Columbia Road Newark, DE 19714",
	"phone": 6266786789
}

expect something like this:
{
	"name": "Craig E. Liriano",
	"address": "3538 Columbia Road Newark, DE 19714",
	"phone": 6266786789,
	"id": 3
}

List of Sales Records - 	"GET" 	request to 	http://localhost:8090/api/sales/
Create a New Sales Record - "POST" 	request to 	http://localhost:8090/api/sales/

try adding something like this:

** Poller runs every 60 seconds to grab the automobileVO, so if you are adding automobile into inventory api, wait until it grabs the information

{
	"automobile": "4Y1SL65848Z411439",
	"sales_person": 12131,
	"customer": 3204340236,
	"price": 30000
}

expect something like this:
{
	"automobile": {
		"color": "Red",
		"year": 2022,
		"vin": "4Y1SL65848Z411439",
		"import_href": "/api/automobiles/4Y1SL65848Z411439/",
		"status": "SOLD"
	},
	"sales_person": {
		"name": "Patrick Star ",
		"employee_number": 12131,
		"id": 2
	},
	"customer": {
		"name": "Shannon B. Clark",
		"address": "2902 Brighton Circle Road Saint Cloud, MN 56303",
		"phone": 3204340236,
		"id": 1
	},
	"price": 30000,
	"id": 21
}

Show Seller History - "GET" request to http://localhost:8090/api/sales/<int:pk>

** replace <int:pk> with sales person ID

try adding something like this:

http://localhost:8090/api/sales/2

expect something like this:
{
	"sales": [
		{
			"automobile": {
				"color": "White",
				"year": 2009,
				"vin": "TEST123",
				"import_href": "/api/automobiles/TEST123/",
				"status": "SOLD"
			},
			"sales_person": {
				"name": "Patrick Star ",
				"employee_number": 12131,
				"id": 2
			},
			"customer": {
				"name": "Shannon B. Clark",
				"address": "2902 Brighton Circle Road Saint Cloud, MN 56303",
				"phone": 3204340236,
				"id": 1
			},
			"price": 50000,
			"id": 17
		},
		{
			"automobile": {
				"color": "Red",
				"year": 2022,
				"vin": "4Y1SL65848Z411439",
				"import_href": "/api/automobiles/4Y1SL65848Z411439/",
				"status": "SOLD"
			},
			"sales_person": {
				"name": "Patrick Star ",
				"employee_number": 12131,
				"id": 2
			},
			"customer": {
				"name": "Shannon B. Clark",
				"address": "2902 Brighton Circle Road Saint Cloud, MN 56303",
				"phone": 3204340236,
				"id": 1
			},
			"price": 30000,
			"id": 19
		}
	]
}

List of AutomobileVO - "GET" request to http://localhost:8090/api/autos/

** 	this is a value object
	uses a poller to pull new automobile information from inventory api

expect something like this:
[
	{
		"color": "White",
		"year": 2022,
		"vin": "FIRST1VIN",
		"import_href": "/api/automobiles/FIRST1VIN/",
		"status": "SOLD"
	},
	{
		"color": "Red",
		"year": 2021,
		"vin": "SECOND2VIN",
		"import_href": "/api/automobiles/SECOND2VIN/",
		"status": "UNSOLD"
	},
	{
		"color": "Blue",
		"year": 2019,
		"vin": "THIRD3VIN",
		"import_href": "/api/automobiles/THIRD3VIN/",
		"status": "SOLD"
	}
]


Models:

SalesPerson - 	Requires a name and employee number. This is necessary to show who sold the car

PotentialCustomer - 	Requires a name, address, and phone number of a potential customer. The phone number has to be unique. This is necessary to show who bought the car.

AutomobileVO -	This is a value object that uses a poller to pull data from the inventory api. Whenever a new automobile is logged into the inventory api, it will create an automobileVO and populate the color, year, VIN, import_href, and status. This is necessary to show what car was sold.

SalesRecord - Has a foreign key relationship with AutomobileVO, SalesPerson, and PotentialCustomer. This is necessary to track the history and details of all cars sold.
