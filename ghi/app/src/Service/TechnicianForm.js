import React, { useState } from 'react';

const TechnicianForm = () => {
    const [ name, setName ] = useState("");
    const [ employee_number, setEmployee ] = useState("");

    const clearTechnicianForm = () => {
        setName("");
        setEmployee("");
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const techURL = "http://localhost:8080/api/technician/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify({
                name: name,
                employee_number: employee_number,
            }),
            headers: {
                "Content-type": "application/json",
            },
        }
        const response = await fetch(techURL, fetchConfig);
        const data = await response.json();
        if (response.ok) {
            clearTechnicianForm()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Technician</h1>
                    <form id="create-technician-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="name" required type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="employee_number" required type="employee_number" name="employee_number" value={employee_number} onChange={(event) => setEmployee(event.target.value)} id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm
